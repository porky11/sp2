use derive_more::{Add, AddAssign, Neg, Sub, SubAssign};
use ga2::{Bivector, Vector};
use num_traits::{Zero, real::Real};

use std::ops::{Div, DivAssign, Mul, MulAssign};

#[derive(Copy, Clone, Debug, Add, AddAssign, Sub, SubAssign, Neg, PartialEq, Eq)]
pub struct Movement<T> {
    pub velocity: Vector<T>,
    pub rotation: Bivector<T>,
}

impl<T: Zero> Zero for Movement<T> {
    fn zero() -> Self {
        Self {
            velocity: Vector::zero(),
            rotation: Bivector::zero(),
        }
    }

    fn is_zero(&self) -> bool {
        self.velocity.is_zero() && self.rotation.is_zero()
    }
}

impl<T: Real> Mul<T> for Movement<T> {
    type Output = Self;

    fn mul(self, other: T) -> Self {
        Self {
            velocity: self.velocity * other,
            rotation: self.rotation * other,
        }
    }
}

impl<T: Real> Div<T> for Movement<T> {
    type Output = Self;

    fn div(self, other: T) -> Self {
        Self {
            velocity: self.velocity / other,
            rotation: self.rotation / other,
        }
    }
}

impl<T: Real + MulAssign> MulAssign<T> for Movement<T> {
    fn mul_assign(&mut self, other: T) {
        self.velocity *= other;
        self.rotation *= other;
    }
}

impl<T: Real + DivAssign> DivAssign<T> for Movement<T> {
    fn div_assign(&mut self, other: T) {
        self.velocity /= other;
        self.rotation /= other;
    }
}
